package uk.co.spaggetti.apg_android;

public class Apg {

	static {
		try {
			System.loadLibrary("apg");
			System.loadLibrary("apg-jni");
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

	public class CharSet {

		public static final int NUMBER = 0x01;
		public static final int SPECIAL = 0x02;
		public static final int UPPER_CASE = 0x04;
		public static final int LOWER_CASE = 0x08;
		public static final int RESTRICTED_SYMBOL = 0x10;
	}

	/**
	 * Generate a random password.
	 * 
	 * @param minLength
	 * @param maxLength
	 * @param characterSets
	 *            Bitmask of character sets to use from {@link CharSet}.
	 * @return
	 * @see Apg#generatePronouceable(int, int, int)
	 */
	public static native String generateRandom(int minLength, int maxLength, int characterSets);

	/**
	 * Generates a pronounceable password.
	 * 
	 * @param minLength
	 * @param maxLength
	 * @param characterSets
	 *            Bitmask of character sets to use from {@link CharSet}.
	 *            Note that {@link CharSet#LOWER_CASE} is always used,
	 *            regardless of the flag specified here.
	 * @return An array containing the password with and without pronunciation
	 *         hyphens, e.g. {@code ['sandup', 'san-dup']}.
	 * @see Apg#generateRandom(int, int, int)
	 */
	public static native String[] generatePronouceable(int minLength, int maxLength,
			int characterSets);
}
