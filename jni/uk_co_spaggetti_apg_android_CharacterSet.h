/* DO NOT EDIT THIS FILE - it is machine generated */
#include <jni.h>
/* Header for class uk_co_spaggetti_apg_android_CharacterSet */

#ifndef _Included_uk_co_spaggetti_apg_android_CharacterSet
#define _Included_uk_co_spaggetti_apg_android_CharacterSet
#ifdef __cplusplus
extern "C" {
#endif
#undef uk_co_spaggetti_apg_android_CharacterSet_NUMERIC
#define uk_co_spaggetti_apg_android_CharacterSet_NUMERIC 1L
#undef uk_co_spaggetti_apg_android_CharacterSet_SPECIAL
#define uk_co_spaggetti_apg_android_CharacterSet_SPECIAL 2L
#undef uk_co_spaggetti_apg_android_CharacterSet_UPPER_CASE
#define uk_co_spaggetti_apg_android_CharacterSet_UPPER_CASE 4L
#undef uk_co_spaggetti_apg_android_CharacterSet_LOWER_CASE
#define uk_co_spaggetti_apg_android_CharacterSet_LOWER_CASE 8L
#undef uk_co_spaggetti_apg_android_CharacterSet_RESTRICTED_SYMBOLS
#define uk_co_spaggetti_apg_android_CharacterSet_RESTRICTED_SYMBOLS 16L
#ifdef __cplusplus
}
#endif
#endif
