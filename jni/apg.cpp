#include <jni.h>
#include <stdio.h>
#include "uk_co_spaggetti_apg_android_Apg.h"

extern "C" {
#include "apg/randpass.h"
#include "apg/pronpass.h"
}

void ThrowRuntimeException(JNIEnv *env, const char* message);

/*
 * Class:     uk_co_spaggetti_apg_android_Apg
 * Method:    generateRandom
 * Signature: (III)Ljava/lang/String;
 */
JNIEXPORT jstring JNICALL Java_uk_co_spaggetti_apg_1android_Apg_generateRandom(
		JNIEnv *env, jclass jClazz, jint jMinLength, jint jMaxLength,
		jint jCharacterSets) {

	char password[jMaxLength + 1];

	int result = gen_rand_pass(password, jMinLength, jMaxLength,
			jCharacterSets);

	if (result == -1) {
		ThrowRuntimeException(env, "Password generation failed.");
		return NULL;
	}

	return env->NewStringUTF(password);
}

/*
 * Class:     uk_co_spaggetti_apg_android_Apg
 * Method:    generatePronouceable
 * Signature: (III)Ljava/lang/String;
 */
JNIEXPORT jobjectArray JNICALL Java_uk_co_spaggetti_apg_1android_Apg_generatePronouceable(
		JNIEnv *env, jclass jClazz, jint jMinLength, jint jMaxLength,
		jint jCharacterSets) {

	char word[jMaxLength + 1];

	/*
	 * Buffer for hypenated word. This is larger than the word itself
	 * as the doc string seems to contradict itself: the returned word
	 * length is inclusive of maxLength, so would be accessing bad memory
	 * if the hypenated buffer was of equal size.
	 */
	char hypenatedWord[(int) (1.5f * (jMaxLength + 1))];

	int result = gen_pron_pass(word, hypenatedWord, jMinLength, jMaxLength,
			jCharacterSets);

	if (result == -1) {
		ThrowRuntimeException(env, "Password generation failed.");
		return NULL;
	}

	// Return word and hypenated word in an array
	jstring jWord = env->NewStringUTF(word);
	jstring jHypenatedWord = env->NewStringUTF(hypenatedWord);
	jobjectArray jWordsArr = env->NewObjectArray(2,
			env->FindClass("java/lang/String"), NULL);
	env->SetObjectArrayElement(jWordsArr, 0, jWord);
	env->SetObjectArrayElement(jWordsArr, 1, jHypenatedWord);
	return jWordsArr;
}

void ThrowRuntimeException(JNIEnv *env, const char* message) {
	jclass clazz = env->FindClass("java/lang/RuntimeException");
	env->ThrowNew(clazz, message);
}
