LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := apg
LOCAL_SRC_FILES :=  \
apg/pronpass.c \
apg/randpass.c \
apg/rnd.c \
apg/errors.c \
apg/convert.c \
apg/cast/cast.c 

include $(BUILD_SHARED_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE    := apg-jni
LOCAL_SRC_FILES := apg.cpp
LOCAL_SHARED_LIBRARIES := apg

include $(BUILD_SHARED_LIBRARY)
